# -*- coding: utf8 -*-
from django.db import models

# Create your models here.
class Personas(models.Model):
    cedula = models.PositiveIntegerField(verbose_name=u'cédula', unique=True)
    primer_nombre = models.CharField(max_length=20)
    segundo_nombre = models.CharField(max_length=20, blank=True)
    primer_apellido = models.CharField(max_length=20)
    segundo_apellido = models.CharField(max_length=20, blank=True)
    sexo =  models.IntegerField(choices=((0,'femenino'),(1,'masculino')), default=1)
    email = models.EmailField()
    tlf = models.CharField(max_length=15, blank=True)
    class Meta:
        db_table = 'personas'
        verbose_name_plural = 'personas'
    def __unicode__(self):
        return '%d - %s %s' %(self.cedula, self.primer_nombre, self.primer_apellido)

class Carreras(models.Model):
    nombre = models.CharField(max_length=30)
    class Meta:
        db_table = 'carreras'
        verbose_name_plural = 'carreras'
    def __unicode__(self):
        return '%s' %(self.nombre)

class Estudiantes(models.Model):
    persona = models.ForeignKey('Personas')
    carrera = models.ForeignKey('Carreras', null=True, blank=True)
    horario = models.ForeignKey('Horario')
    class Meta:
        db_table = 'estudiantes'
        verbose_name_plural = 'estudiantes'
    def __unicode__(self):
        return u'%s %s' %(self.persona, self.carrera)

class Horario(models.Model):
    materia = models.CharField(max_length=15)
    seccion = models.CharField(max_length=5, verbose_name=u'sección')
    nota = models.FloatField(blank=True, null=True)
    class Meta:
        db_table = 'horario'
    def __unicode__(self):
        return '%s - %s %s' %(self.materia, self.seccion, self.nota)

class HorarioDetalle(models.Model):
    horario = models.ForeignKey('Horario')
    aula = models.CharField(max_length=10)
    cupos = models.IntegerField()
    class Meta:
        db_table = 'horario_detalles'
        verbose_name_plural = 'horario detalles'
    def __unicode__(self):
        return '%s %d' %(self.aula, self.cupos)
