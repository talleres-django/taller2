# Create your views here.
from django.shortcuts import render_to_response
from notas.models import Carreras, Personas
from notas.forms import CarrerasForm, PersonasForm
from django.core.context_processors import csrf

def personas(request):
    diccionario = {}
    diccionario.update(csrf(request))

    form = PersonasForm()
    import pdb
    if request.method == 'POST':
        diccionario.update({'formulario':form})
        if form.is_valid():
            cedula = request.POST['cedula']
            p_nombre = request.POST['primer_nombre']
            s_nombre = request.POST['segundo_nombre']
            p_apellido = request.POST['primer_apellido']
            s_apellido = request.POST['segundo_apellido']
            sexo = request.POST['sexo']
            email = request.POST['email']
            tlf = request.POST['tlf']
            persona = Personas.objects.create(cedula=cedula,primer_nombre=p_nombre,segundo_nombre=s_nombre,primer_apellido=p_apellido,segundo_apellido=s_apellido, sexo=sexo, email=email, tlf=tlf)
            mensaje = "Guardado exitosamente"
            diccionario.update({'mensaje':mensaje})
            return render_to_response('formulario_personas.html', diccionario)
        else:
            mensaje = "Formulario incorrectamente llenado"
            diccionario.update({'mensaje':mensaje})
            return render_to_response('formulario_personas.html', diccionario)
    else:
        diccionario.update({'formulario':form})
        mensaje = "Bienvenido."
        diccionario.update({'mensaje':mensaje})
        return render_to_response('formulario_personas.html', diccionario)


def horario(request):
    print "Horario"
    import pdb

    diccionario = {}
    diccionario.update(csrf(request))

    if request.method == 'POST':
        crear_carrera = Carreras.objects.filter(nombre=request.POST['nombre'])
        form = CarrerasForm()
        if crear_carrera.exists():
            diccionario.update({'formulario':form})
            diccionario.update({'mensaje':'La carrera %s ya existe actualmente'%(crear_carrera[0].nombre)})
            return render_to_response('crear_carrera.html', diccionario)
        else:
            crear_carrera = Carreras.objects.create(nombre=request.POST['nombre'])
            diccionario.update({'formulario':form})
            diccionario.update({'mensaje':'La carrera %s ha sido agregada exitosamente' %(crear_carrera.nombre)} )
            return render_to_response('crear_carrera.html', diccionario)
    else:
        form = CarrerasForm()
        diccionario.update({'formulario':form})
        return render_to_response('crear_carrera.html', diccionario)
