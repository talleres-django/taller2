# -*- coding: utf8 -*-
from django.forms import Form, CharField, ModelForm, IntegerField
from notas.models import Personas

class CarrerasForm(Form):
    nombre = CharField()

class PersonasForm(ModelForm):
    edad = IntegerField(error_messages={'invalid':u"Este no es un número entero.", 'required':'Este es un campo obligatoriamente obligado! '})
    class Meta:
        model = Personas
        exclude = ('segundo_nombre', 'segundo_apellido')
        #include = ('campo1', 'campo2')
