from django.contrib import admin
from notas.models import Personas, Carreras, Estudiantes, Horario, HorarioDetalle

class PersonasAdmin(admin.ModelAdmin):
    list_display = ('cedula', 'primer_nombre', 'segundo_nombre', 'primer_apellido', 'segundo_apellido', 'sexo', 'email', 'tlf')
    search_fields = ('cedula', 'primer_nombre', 'segundo_nombre', 'primer_apellido', 'segundo_apellido', 'email', 'tlf')
    list_filter = ('sexo',)
admin.site.register(Personas, PersonasAdmin)
admin.site.register(Carreras)
admin.site.register(Estudiantes)
admin.site.register(HorarioDetalle)

class HorarioDetalleInline(admin.TabularInline):
    model = HorarioDetalle
    extra = 1

class HorarioAdmin(admin.ModelAdmin):
    list_display = ('materia', 'seccion', 'nota')
    search_fields = ('materia', 'seccion')
    inlines = (HorarioDetalleInline,)
admin.site.register(Horario, HorarioAdmin)
